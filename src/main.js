import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

import MainRoute from './App.vue'
import home from './pages/Home.vue';
const router = new VueRouter({
    mode:'history',
    routes:[
        {
            path:'/',
            redirect: "/home",

        },
        {
            path:'/home',
            name:"home",
            component: home,
        },
        {
            path:"*",
            redirect:"/home",
        }
    ]
});

Vue.config.productionTip = false

new Vue({
    router,
    render: h => h(MainRoute)

}).$mount('#app');